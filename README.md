# Week10-mini

## Requirements
This project implements a Rust Serverless Transformer. We need to dockerize Hugging Face Rust transformer, deploy container to AWS Lambda,and implement query endpoint.

## GEtting start
1. Create a new cargo lambda project running `cargo lambda new week10-mini`
2. implemented the function in main.rs
3. run `cargo lambda watch` and visit localhost:8080 to see:
![](pic/1.png)

4. Create a new ECR repo and upload the image
![](pic/2.png)

5. Create a new lambda function using the given ECR repo
![](pic/3.png)
 